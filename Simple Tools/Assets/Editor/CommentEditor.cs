﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Comment))]
public class CommentEditor : Editor {

private static float INIT_HEIGHT = 56;

public override void OnInspectorGUI() {
	Comment comment = target as Comment;
	GUIStyle style = new GUIStyle(EditorStyles.textField);
	style.wordWrap = true;
	GUILayoutOption[] layout = new GUILayoutOption[2] {GUILayout.ExpandHeight(true),
	  GUILayout.MaxHeight(INIT_HEIGHT)};
	string text = EditorGUILayout.TextArea(comment.text, style, layout);
	if (text != comment.text) {
		Undo.RegisterUndo(target, "Comment on " + comment.gameObject.name);
		comment.text = text;
		EditorUtility.SetDirty(target);
	}
}

}

